import numpy as np

# to define array with integers
normal_array = np.array([1,2,3,4,5], dtype=int)
print(normal_array)

# if want to create array with random values in a range
random_array = np.random.randint(2, 100, size=(4,10))
print(random_array)

# create a array in range of given numbers 
aranged_array = np.arange(40).reshape(4,10)
print(aranged_array)

# to reshape the array
reshape_array = random_array.reshape(10,4)
print(reshape_array)

# to sort the array 
reshape_array.sort()
print(reshape_array)

# to find mininmum of the array 
print(reshape_array.min())

# to find maximum of the array
print(reshape_array.max())


# to sum the array 
print(np.mean(reshape_array, axis=0))


# to find the std deviation along y-axis 
print(np.std(reshape_array, axis=1))

# to find the std deviation along x-axis
print(np.std(reshape_array, axis=0))

# to find the variance of the array
print(np.var(reshape_array))

# to find the corelation coefficient of the array 
print(np.corrcoef(reshape_array))

# median of the array

print(np.median(reshape_array))


# mathematical operations 
array1 = np.array([1,2,3,4]).reshape(2,2)
array2 = np.array([5,6,7,8]).reshape(2,2) 


# addition
print(np.add(array1, array2))

#subtraction
print(np.subtract(array1, array2))

#multiply
print(np.multiply(array1, array2))

#cross product
print(np.cross(array1, array2))

#dot product
print(np.dot(array1, array2))


#multiplicatiion by the number
print(array1*10)

# sqrt of each element

print(np.sqrt(array1))

# sin and log

print(np.sin(array1))
print(np.log(array1))



# to convert multidimentional array into flat array 
flat_array = reshape_array.flatten()
print(flat_array)


# array comparison
# compare all elements of the array 
print(array1 > array2)

# checks whether array has a element or not for this case it is 2
print(array1 == 2)


#slicing operations 

print(array1[:1])